import * as request from 'superagent';

export const fetchTodos = (filter) =>
  request
    .get('todos')
    .set('Content-Type', 'application/json')
    .query({ f: filter })
    .then(res => {
      if (res.ok) return res.body;
    });

export const addTodo = (text) =>
  request
    .post('todos')
    .send({ text })
    .then(res => {
      if (res.ok) return res.body;
    });

export const toggleTodo = (id, completed) =>
  request
    .put('todos')
    .send({ id, completed })
    .then(res => {
      if (res.ok) return res.body;
    });

export const saveTodo = (id, text) =>
  request
    .put('todos')
    .send({ id, text })
    .then(res => {
      if (res.ok) return res.body;
    });

export const deleteTodo = (id) =>
  request
    .del('todos')
    .send({ id })
    .then(res => {
      if (res.ok) return res.body;
    });