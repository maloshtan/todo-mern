import test from 'tape';
import { put, call } from 'redux-saga/effects';

import { fetchTodos } from './index';
import * as api from '../../api';
import constants from '../constants';

test('fetchTodos Saga test', (assert) => {
  const gen = fetchTodos({
    type: constants.FETCH_TODOS_REQUEST,
    filter: 'all'
  });

  assert.deepEqual(
    gen.next().value,
    call(api.fetchTodos, 'all'),
    'fetchTodos Saga must call api.fetchTodos("all")'
  );

  assert.end();
})