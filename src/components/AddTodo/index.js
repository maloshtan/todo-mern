import React from 'react';
import { connect } from 'react-redux';

import { addTodo } from '../../redux/actions';

const AddTodo = ({ dispatch }) => {
  let input;

  const dispatchAddTodo = () => {
    if (!input.value) {
      return;
    }
    dispatch(addTodo(input.value));
    input.value = '';
  }

  return (
    <div>
      <input
        ref={node => input = node}
        onKeyPress={e => {
          if(e.key === 'Enter') {
            dispatchAddTodo();
          }
        }}
      />
      <button
        onClick={dispatchAddTodo}
      >
        Add Todo
      </button>
    </div>
  );
};

export default connect()(AddTodo);