import React from 'react';

const Todo = ({
  text,
  completed,
  isEdited,
  onClick,
  onDoubleClick,
  onSave,
  onCancel,
  onDelete,
}) => {
  let input;
  return (
    <li
      style={{
        textDecoration: completed ? 'line-through' : 'none'
      }}
    >
      <input
        type="checkbox"
        checked={completed}
        onChange={onClick} />
      {
        !isEdited ?
          <span onDoubleClick={(onDoubleClick)}>{text}</span> :
          <span>
            <input
              ref={node => input = node}
              type='text'
              defaultValue={text}
            />
            <button onClick={() => onSave(input.value)}>Save</button>
            <button onClick={onCancel}>Cancel</button>
          </span>
      }
      <button
        style={{ 'marginLeft': '10px', 'color': 'red' }}
        onClick={onDelete}
      > x </button>
    </li>
  )
};

export default Todo;