const merge = require('webpack-merge');
const config = require('./webpack.config.js');
const Visualizer = require('webpack-visualizer-plugin');

module.exports = merge(config, {
  plugins: [
    new Visualizer({
      filename: './stats.html'
    })
  ],
  devtool: 'inline-source-map',
});