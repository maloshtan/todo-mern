const express = require('express');
const bodyParser = require('body-parser');
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
require('dotenv').config();

const app = express();
let db;

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('public'));

const port = process.env.PORT || 8080;

MongoClient.connect(
  process.env.DB_LINK,
  (err, client) => {
    if (err) return console.log(err);
    db = client.db(process.env.DB_NAME);
    app.listen(port, () => {
      console.log('port 8080');
    });
  }
);

app.get(['/', '/active', '/completed'], (req, res) => {

  res.sendFile(__dirname + '/public' + '/index.html');
});

app.post('/todos', (req, res) => {
  const newTodo = {
    ...req.body,
    get id() {
      return this._id
    },
    completed: false
  };

  db.collection('todos').save(newTodo, (err, result) => {
    if (err) return console.log(err);

    res.send(result.ops[0]);
  })
});

app.get('/todos', (req, res) => {

  const {f: filter} = req.query;

  const dbQuery = {
    all: {},
    active: { completed: false},
    completed: { completed: true}
  };

  db.collection('todos').find(dbQuery[filter]).toArray(
    (err, results) => {
      if (err) return console.log(err);
      res.send(results);
    }
  );
});

app.put('/todos', (req, res) => {

  let update = {};
  let keysExceptID = Object.keys(req.body).filter(key => key !== 'id');
  keysExceptID.forEach(key => update[key] = req.body[key]);

  db.collection('todos').findOneAndUpdate(
    {id: ObjectID(req.body.id)},
    {
      $set: update
    },
    {
      upsert: false,
      returnOriginal: false
    },
    (err, result) => {
      if (err) return console.log(err);
      res.send(result.value);
    }
  );
});

app.delete('/todos', (req, res) => {
  db.collection('todos').findOneAndDelete(
    {id: ObjectID(req.body.id)},
    (err, result) => {
      if (err) return res.send(500, err);
      res.send(result.value.id);
    }
  )
})